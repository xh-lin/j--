package jminusminus;

import java.util.ArrayList;

import static jminusminus.CLConstants.ALOAD_0;
import static jminusminus.CLConstants.INVOKESPECIAL;
import static jminusminus.CLConstants.RETURN;
import static jminusminus.TokenKind.*;

/**
 * A representation of an interface declaration.
 */
public class JInterfaceDeclaration extends JAST implements JTypeDecl {
    // Class modifiers.
    private final ArrayList<String> mods;
    // Class name.
    private final String name;
    // Super class types.
    private final ArrayList<Type> superTypes;
    // Class block.
    private final ArrayList<JMember> interfaceBlock;
    // This class type.
    private Type thisType;
    // Context for this class.
    private ClassContext context;

    /**
     * Constructs an AST node for an interface declaration.
     *
     * @param line       line in which the interface declaration occurs in the source file.
     * @param mods       class modifiers.
     * @param name       class name.
     * @param superTypes  super class types.
     * @param interfaceBlock interface block.
     */
    public JInterfaceDeclaration(int line, ArrayList<String> mods, String name,
                                 ArrayList<Type> superTypes,
                                 ArrayList<JMember> interfaceBlock) {
        super(line);
        this.mods = mods;
        this.name = name;
        this.superTypes = superTypes;
        this.interfaceBlock = interfaceBlock;
        mods.add(INTERFACE.image());
        mods.add(ABSTRACT.image());
    }

    /**
     * {@inheritDoc}
     */
    public void declareThisType(Context context) {
        String qualifiedName = JAST.compilationUnit.packageName().equals("") ?
                name : JAST.compilationUnit.packageName() + "/" + name;
        CLEmitter partial = new CLEmitter(false);
        partial.addClass(mods, qualifiedName, Type.OBJECT.jvmName(), null, false);
        thisType = Type.typeFor(partial.toClass());
        context.addType(line, thisType);
    }

    /**
     * {@inheritDoc}
     */
    public void preAnalyze(Context context) {
        // Construct a class context.
        this.context = new ClassContext(this, context);

        // Resolve superclasses.
        for (int i = 0; i < superTypes.size(); i++) {
            Type superType = superTypes.get(i).resolve(this.context);
            superTypes.set(i, superType);
        }

        // Creating a partial class in memory can result in a java.lang.VerifyError if the
        // semantics below are violated, so we can't defer these checks to analyze().
        for (Type superType : superTypes) {
            thisType.checkAccess(line, superType);
            if (superType.isFinal()) {
                JAST.compilationUnit.reportSemanticError(line, "Cannot extend a final type: %s",
                        superType.toString());
            }
        }

        // Create the (partial) class.
        CLEmitter partial = new CLEmitter(false);

        // Add the class header to the partial class
        String qualifiedName = JAST.compilationUnit.packageName().equals("") ?
                name : JAST.compilationUnit.packageName() + "/" + name;
        for (Type superType : superTypes)
            partial.addClass(mods, qualifiedName, superType.jvmName(), null, false);

        // Pre-analyze the members and add them to the partial class.
        for (JMember member : interfaceBlock) {
            // add public and abstract modifiers implicitly
            if (member instanceof JMethodDeclaration)
                ((JMethodDeclaration) member).setOfInterface();
            member.preAnalyze(this.context, partial);
        }

        // Get the ClassRep for the (partial) class and make it the representation for this type.
        Type id = this.context.lookupType(name);
        if (id != null && !JAST.compilationUnit.errorHasOccurred()) {
            id.setClassRep(partial.toClass());
        }
    }

    /**
     * {@inheritDoc}
     */
    public String name() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    public Type thisType() {
        return thisType;
    }

    /**
     * {@inheritDoc}
     */
    public Type superType() {
        return null;
    }

    public ArrayList<Type> superTypes() {
        return superTypes;
    }

    /**
     * {@inheritDoc}
     */
    public JAST analyze(Context context) {
        // Analyze all members
        for (JMember member : interfaceBlock) {
            ((JAST) member).analyze(this.context);
        }

        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
        // The class header.
        String qualifiedName = JAST.compilationUnit.packageName() == "" ?
                name : JAST.compilationUnit.packageName() + "/" + name;
        for (Type superType : superTypes)
            output.addClass(mods, qualifiedName, superType.jvmName(), null,
                    false);

        // The members.
        for (JMember member : interfaceBlock) {
            ((JAST) member).codegen(output);
        }

    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JInterfaceDeclaration:" + line, e);
        if (mods != null) {
            ArrayList<String> value = new ArrayList<String>();
            for (String mod : mods) {
                value.add(String.format("\"%s\"", mod));
            }
            e.addAttribute("modifiers", value);
        }
        e.addAttribute("name", name);
        if (superTypes != null) {
            ArrayList<String> typenames = new ArrayList<String>();
            for (Type t : superTypes)
                typenames.add('"' + t.toString() + '"');
            e.addAttribute("super", typenames);
        }
        if (interfaceBlock != null) {
            for (JMember member : interfaceBlock) {
                ((JAST) member).toJSON(e);
            }
        }
    }
}
