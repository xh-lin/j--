package jminusminus;

import static jminusminus.CLConstants.GOTO;

/**
 * The AST node for a break statement.
 */
class JBreakStatement extends JStatement {
    private JStatement enclosure;

    /**
     * Constructs an AST node for a break statement given its line number,
     *
     * @param line line in which the break statement occurs in the source file.
     */
    public JBreakStatement(int line) {
        super(line);
    }

    /**
     * {@inheritDoc}
     */
    public JStatement analyze(Context context) {
        enclosure = JMember.controlFlows.peek();
        if (enclosure == null) {
            JAST.compilationUnit.reportSemanticError(line(),
                    "break statement is outside of a control flow");
        }
        return this;
    }

    /**
     * Label needs to be set up in advance.
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
        String label = null;
        if (enclosure instanceof JDoStatement) {
            label = ((JDoStatement) enclosure).getBreakLabel();
        } else if (enclosure instanceof JWhileStatement) {
            label = ((JWhileStatement) enclosure).getBreakLabel();
        } else if (enclosure instanceof JForStatement) {
            label = ((JForStatement) enclosure).getBreakLabel();
        } else if (enclosure instanceof JSwitchStatement) {
            label = ((JSwitchStatement) enclosure).getBreakLabel();
        } else {
            JAST.compilationUnit.reportSemanticError(line(),
                    "break statement should not be inside of " + enclosure.toString());
        }
        output.addBranchInstruction(GOTO, label);
    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JBreakStatement:" + line, e);
    }
}
