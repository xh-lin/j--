package jminusminus;

import java.util.ArrayList;
import static jminusminus.CLConstants.*;

/**
 * The AST node for a try statement.
 */
class JTryStatement extends JStatement {
    private JBlock tblock;
    private ArrayList<JFormalParameter> cparameters;
    private ArrayList<JBlock> cblocks;
    private JBlock fblock;
    private Context context;
    private ArrayList<LocalContext> catchContexts;

    /**
     * Constructs an AST node for a try statement given its line number,
     *
     * @param line line in which the try statement occurs in the source file.
     * @param tblock the AST node for try block
     * @param cparameters the AST node for catch parameters
     * @param cblocks the AST node for catch blocks
     * @param fblock the AST node for finally block
     */
    public JTryStatement(int line, JBlock tblock, ArrayList<JFormalParameter> cparameters,
                         ArrayList<JBlock> cblocks, JBlock fblock) {
        super(line);
        this.tblock = tblock;
        this.cparameters = cparameters;
        this.cblocks = cblocks;
        this.fblock = fblock;
        catchContexts = new ArrayList<LocalContext>();
    }

    /**
     * {@inheritDoc}
     */
    public JStatement analyze(Context context) {
        this.context = context;
        // Analyze the try block
        tblock = tblock.analyze(context);
        int i = 0;
        for (JFormalParameter p : cparameters) {
            LocalContext localContext = new LocalContext(context);
            this.catchContexts.add(localContext);
            // declare each catch parameter in the new local context
            p.setType(p.type().resolve(localContext));
            int offset = localContext.nextOffset();
            LocalVariableDefn defn = new LocalVariableDefn(p.type(), offset);
            defn.initialize();
            localContext.addEntry(p.line(), p.name(), defn);
            // analyze each catch block in the new local context
            JBlock cblock = cblocks.get(i).analyze(localContext);
            cblocks.set(i++, cblock);
        }
        // Analyze the finally block
        if (fblock != null)
            fblock = fblock.analyze(context);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
        String startTry = output.createLabel();
        String endTry = output.createLabel();
        String startFinally = output.createLabel();
        String startFinallyPlusOne = output.createLabel();
        String endFinally = output.createLabel();
        ArrayList<String> handlerLabels = new ArrayList<String>();
        ArrayList<String> catchTypes = new ArrayList<String>();

        // try
        output.addLabel(startTry);
        tblock.codegen(output);
        if (fblock != null)
            fblock.codegen(output);
        output.addBranchInstruction(GOTO, endFinally);

        output.addLabel(endTry);

        // catches
        int b = 0;
        for (JFormalParameter p : cparameters) {
            String handlerLabel = output.createLabel();
            handlerLabels.add(handlerLabel);
            catchTypes.add(context.lookupType(p.type().toString()).jvmName());
            output.addLabel(handlerLabel);
            int offset = ((LocalVariableDefn) catchContexts.get(b).lookup(p.name())).offset();
            switch (offset) {
                case 0:
                    output.addNoArgInstruction(ASTORE_0);
                    break;
                case 1:
                    output.addNoArgInstruction(ASTORE_1);
                    break;
                case 2:
                    output.addNoArgInstruction(ASTORE_2);
                    break;
                case 3:
                    output.addNoArgInstruction(ASTORE_3);
                    break;
                default:
                    output.addOneArgInstruction(ASTORE, offset);
            }
            (cblocks.get(b++)).codegen(output);
            if (fblock != null)
                fblock.codegen(output);
            output.addBranchInstruction(GOTO, endFinally);
        }

        // finally
        output.addLabel(startFinally);
        output.addOneArgInstruction(ASTORE, tblock.offset());
        output.addLabel(startFinallyPlusOne);
        if (fblock != null)
            fblock.codegen(output);
        output.addOneArgInstruction(ALOAD, tblock.offset());
        output.addNoArgInstruction(ATHROW);

        output.addLabel(endFinally);

        // Add exception handlers
        for (int i = 0; i < handlerLabels.size(); i++)
            output.addExceptionHandler(startTry, endTry, handlerLabels.get(i), catchTypes.get(i));
        output.addExceptionHandler(startTry, endTry, startFinally, null);
        handlerLabels.add(startFinally);
        handlerLabels.add(startFinallyPlusOne);
        for (int i = 0; i < handlerLabels.size()-1; i++) {
            output.addExceptionHandler(handlerLabels.get(i), handlerLabels.get(i+1), startFinally,
                    null);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JTryStatement:" + line, e);
        JSONElement tb = new JSONElement();
        e.addChild("TryBlock", tb);
        tblock.toJSON(tb);
        int i = 0;
        for (JFormalParameter p : cparameters) {
            JSONElement cb = new JSONElement();
            e.addChild("CatchBlock", cb);
            ArrayList<String> value = new ArrayList<String>();
            value.add('"' + p.name() + '"');
            value.add('"' + p.type().toString() + '"');
            cb.addAttribute("parameter", value);
            (cblocks.get(i++)).toJSON(cb);
        }
        if (fblock != null) {
            JSONElement fb = new JSONElement();
            e.addChild("FinallyBlock", fb);
            fblock.toJSON(fb);
        }

    }
}
