package jminusminus;

import static jminusminus.CLConstants.*;

/**
 * The AST node for a long literal.
 */
class JLiteralLong extends JExpression {
    // String representation of the literal.
    private final String text;

    /**
     * Constructs an AST node for an long literal given its line number and string representation.
     *
     * @param line line in which the literal occurs in the source file.
     * @param text string representation of the literal.
     */
    public JLiteralLong(int line, String text) {
        super(line);
        this.text = text;
    }

    /**
     * {@inheritDoc}
     */
    public JExpression analyze(Context context) {
        type = Type.LONG;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
        // remove L or l
        long l;
        int len = text.length()-1;
        char last = text.charAt(len);
        if (last == 'L' || last == 'l')
            l = Long.parseLong(text.substring(0, len));
        else
            l = Long.parseLong(text);
        if (l == 0) {
            output.addNoArgInstruction(LCONST_0);
        } else if (l == 1) {
            output.addNoArgInstruction(LCONST_1);
        } else {
            output.addLDCInstruction(l);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JLiteralLong:" + line, e);
        e.addAttribute("type", type == null ? "" : type.toString());
        e.addAttribute("value", text);
    }
}
