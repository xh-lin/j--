package jminusminus;

import static jminusminus.CLConstants.GOTO;

/**
 * The AST node for a conditional expression.
 */
class JConditionalExpression extends JExpression {
    private JExpression condition, thenPart, elsePart;

    /**
     * Constructs an AST node for a conditional expression given its line number, condition,
     * then-part, and else-part
     *
     * @param line line in which the conditional expression occurs in the source file.
     * @param condition the AST node for condition
     * @param thenPart the AST node for then-part
     * @param elsePart the AST node for else-part
     */
    public JConditionalExpression(int line, JExpression condition, JExpression thenPart,
                                  JExpression elsePart) {
        super(line);
        this.condition = condition;
        this.thenPart = thenPart;
        this.elsePart = elsePart;
    }

    /**
     * {@inheritDoc}
     */
    public JExpression analyze(Context context) {
        condition = (JExpression) condition.analyze(context);
        condition.type().mustMatchExpected(line(), Type.BOOLEAN);
        thenPart = (JExpression) thenPart.analyze(context);
        elsePart = (JExpression) elsePart.analyze(context);
        thenPart.type().mustMatchExpected(line(), elsePart.type());
        type = thenPart.type();
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
        String elseLabel = output.createLabel();
        String endLabel = output.createLabel();
        condition.codegen(output, elseLabel, false);
        thenPart.codegen(output);
        output.addBranchInstruction(GOTO, endLabel);
        output.addLabel(elseLabel);
        elsePart.codegen(output);
        output.addLabel(endLabel);
    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        JSONElement c = new JSONElement();
        JSONElement tp = new JSONElement();
        JSONElement ep = new JSONElement();
        json.addChild("JConditionalExpression:" + line, e);
        e.addChild("Condition", c);
        e.addChild("ThenPart", tp);
        e.addChild("ElsePart", ep);
        condition.toJSON(c);
        thenPart.toJSON(tp);
        elsePart.toJSON(ep);
    }
}
