package jminusminus;

import java.util.ArrayList;
import java.util.HashMap;

import static jminusminus.CLConstants.GOTO;

/**
 * The AST node for a for statement.
 */
public class JForStatement extends JStatement{
    private HashMap<ArrayList<JStatement>, JVariableDeclaration> init;
    private JExpression condition;
    private ArrayList<JStatement> update;
    private JStatement body;

    private String breakLabel;
    private String continueLabel;


    /**
     * Constructs an AST node for a for statement given its line number,
     *
     * @param line line in which the for statement occurs in the source file.
     * @param init the AST node for the initializations (either contains variable declarators or
     *             statement expressions)
     * @param condition the AST node for the the condition
     * @param update the AST node for the updates
     * @param body the AST node for the body
     */
    public JForStatement(int line,
                         HashMap<ArrayList<JStatement>, JVariableDeclaration> init,
                         JExpression condition, ArrayList<JStatement> update, JStatement body) {
        super(line);
        this.init = init;
        this.condition = condition;
        this.update = update;
        this.body = body;
    }

    /**
     * {@inheritDoc}
     */
    public JStatement analyze(Context context) {
        // for analyzing break and continue statements
        JMember.controlFlows.push((JStatement) this);

        LocalContext localContext = new LocalContext(context);
        if (init != null) {
            HashMap<ArrayList<JStatement>, JVariableDeclaration> new_init =
                    new HashMap<ArrayList<JStatement>, JVariableDeclaration>();
            for (ArrayList<JStatement> k : init.keySet()) {
                if (k != null) {
                    ArrayList<JStatement> new_k = new ArrayList<JStatement>();
                    for (JStatement s : k)
                        new_k.add((JStatement) s.analyze(localContext));
                    new_init.put(new_k, null);
                } else {
                    JVariableDeclaration d = init.get(k);
                    d = (JVariableDeclaration) d.analyze(localContext);
                    new_init.put(null, d);
                }
                break; // there should be only one pair, break just in case
            }
            init = new_init;
        }
        if (condition != null) {
            condition = condition.analyze(localContext);
            condition.type().mustMatchExpected(line(), Type.BOOLEAN);
        }
        if (update != null) {
            for (int i = 0; i < update.size(); i++) {
                JStatement s = (JStatement) update.get(i).analyze(localContext);
                update.set(i, s);
            }
        }
        body = (JStatement) body.analyze(localContext);

        // pop self out
        JMember.controlFlows.pop();

        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
        String test = output.createLabel();
        String out = output.createLabel();
        String begin = output.createLabel();
        setBreakLabel(out);
        setContinueLabel(test);
        if (init != null) {
            for (ArrayList<JStatement> k : init.keySet()) {
                if (k != null) {
                    for (JStatement s : k)
                        s.codegen(output);
                } else {
                    JVariableDeclaration d = init.get(k);
                    d.codegen(output);
                }
                break; // there should be only one pair, break just in case
            }
        }
        output.addBranchInstruction(GOTO, begin);
        output.addLabel(test);
        if (update != null) {
            for (JStatement s : update)
                s.codegen(output);
        }
        output.addLabel(begin);
        if (test != null)
            condition.codegen(output, out, false);
        body.codegen(output);
        output.addBranchInstruction(GOTO, test);
        output.addLabel(out);
    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JForStatement:" + line, e);
        JSONElement i = new JSONElement();
        if (init != null) {
            e.addChild("Init", i);
            for (ArrayList<JStatement> k : init.keySet()) {
                if (k != null) {
                    for (JStatement s : k)
                        s.toJSON(i);
                } else {
                    JVariableDeclaration d = init.get(k);
                    d.toJSON(i);
                }
                break; // there should be only one pair, break just in case
            }
        }
        if (condition != null) {
            JSONElement c = new JSONElement();
            e.addChild("Condition", c);
            condition.toJSON(c);
        }
        if (update != null) {
            JSONElement u = new JSONElement();
            e.addChild("Update", u);
            for (JStatement s : update)
                s.toJSON(u);
        }
        JSONElement b = new JSONElement();
        e.addChild("Body", b);
        body.toJSON(b);
    }

    public void setBreakLabel(String breakLabel) {
        this.breakLabel = breakLabel;
    }

    public String getBreakLabel() {
        return this.breakLabel;
    }

    public void setContinueLabel(String continueLabel) {
        this.continueLabel = continueLabel;
    }

    public String getContinueLabel() {
        return this.continueLabel;
    }
}
