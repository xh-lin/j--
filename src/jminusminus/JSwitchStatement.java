package jminusminus;

import java.util.ArrayList;
import java.util.TreeMap;
import static jminusminus.TokenKind.*;

/**
 * The AST node for a switch statement.
 */
class JSwitchStatement extends JStatement  {
    private JExpression test;
    private final ArrayList<ArrayList<JStatement>> groups;
    private int lo, hi, nlabels;
    private ArrayList<JSwitchLabel> cases;
    private JSwitchLabel defaultCase;
    private enum OpCode {TABLESWITCH, LOOKUPSWITCH}

    private String breakLabel;


    /**
     * Constructs an AST node for a switch statement given its line number, test, groups
     *
     * @param line line in which the switch statement occurs in the source file.
     * @param test the AST node for condition
     * @param groups the AST node for switch statement group
     */
    public JSwitchStatement(int line, JExpression test, ArrayList<ArrayList<JStatement>> groups) {
        super(line);
        this.test = test;
        this.groups = groups;
        cases = new ArrayList<JSwitchLabel>();
    }

    /**
     * {@inheritDoc}
     */
    public JStatement analyze(Context context) {
        // for analyzing break and continue statements
        JMember.controlFlows.push((JStatement) this);

        hi = Integer.MIN_VALUE;
        lo = Integer.MAX_VALUE;
        nlabels = 0;
        int val;
        // Analyze the condition and make sure it is an integer
        test = (JExpression) test.analyze(context);
        test.type().mustMatchExpected(line(), Type.INT);
        // Analyze the statements in each case group
        for (ArrayList<JStatement> group : groups) {
            for (int i = 0; i < group.size(); i++) {
                JStatement statement = (JStatement) group.get(i).analyze(context);
                group.set(i, statement);
                if (statement instanceof JSwitchLabel) {
                    JSwitchLabel switchLabel = (JSwitchLabel) statement;
                    if (switchLabel.type() == CASE) {
                        nlabels++;
                        cases.add(switchLabel);
                        val = switchLabel.value();
                        if (val > hi) hi = val;
                        if (val < lo) lo = val;
                    } else { // DEFLT
                        if (defaultCase == null)
                            defaultCase = switchLabel;
                        else
                            JAST.compilationUnit.reportSemanticError(line(),
                                    "Duplicate default label");
                    }
                }
            }
        }

        // pop self out
        JMember.controlFlows.pop();

        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
        // decide which instruction (TABLESWITCH or LOOKUPSWITCH) to emit
        long table_space_cost = 4 + (( long ) hi - lo + 1);
        long table_time_cost = 3;
        long lookup_space_cost = 3 + 2 * ( long ) nlabels ;
        long lookup_time_cost = nlabels ;
        OpCode opcode = (nlabels > 0 && table_space_cost + 3 * table_time_cost <= lookup_space_cost
            + 3 * lookup_time_cost) ? OpCode.TABLESWITCH : OpCode.LOOKUPSWITCH ;

        // switch part
        String endLabel = output.createLabel();
        setBreakLabel(endLabel);
        String defaltLabel;
        if (defaultCase != null) {
            defaltLabel = output.createLabel();
            defaultCase.setLabel(defaltLabel);
        } else
            defaltLabel = endLabel;
        test.codegen(output);
        if (opcode == OpCode.TABLESWITCH) {
            ArrayList<String> labels = new ArrayList<String>();
            for (JSwitchLabel aCase : cases) {
                aCase.setLabel(output.createLabel());
                labels.add(aCase.getLabel());
            }
            output.addTABLESWITCHInstruction(defaltLabel, lo, hi, labels);
        } else { // LOOKUPSWITCH
            TreeMap<Integer, String> matchLabelPairs = new TreeMap<Integer, String>();
            for (JSwitchLabel aCase : cases) {
                aCase.setLabel(output.createLabel());
                matchLabelPairs.put(aCase.value(), aCase.getLabel());
            }
            output.addLOOKUPSWITCHInstruction(defaltLabel, nlabels, matchLabelPairs);
        }
        for (ArrayList<JStatement> group : groups) {
            for (JStatement statement : group)
                statement.codegen(output);
        }
        output.addLabel(endLabel);
    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JSwitchStatement:" + line, e);
        JSONElement c = new JSONElement();
        e.addChild("Condition", c);
        test.toJSON(c);
        for (ArrayList<JStatement> group : groups) {
            JSONElement g = new JSONElement();
            e.addChild("SwitchStatementGroup", g);
            for (JStatement statement : group) {
                statement.toJSON(g);
            }
        }
    }

    public void setBreakLabel(String breakLabel) {
        this.breakLabel = breakLabel;
    }

    public String getBreakLabel() {
        return this.breakLabel;
    }
}

/**
 * The AST node for a switch label.
 */
class JSwitchLabel extends JStatement {
    private final TokenKind type; // CASE or DEFLT
    private JExpression expression;
    private String label;

    /**
     * Constructs an AST node for a switch label given its line number, type, expression
     *
     * @param line line in which the switch label occurs in the source file.
     * @param type case or default
     * @param expression the AST node for the label expression
     */
    public JSwitchLabel(int line, TokenKind type, JExpression expression) {
        super(line);
        this.type = type;
        this.expression = expression;
    }

    /**
     * {@inheritDoc}
     */
    public JStatement analyze(Context context) {
        if (type == CASE) {
            // Analyze the case expressions and make sure they are integer literals
            expression = (JExpression) expression.analyze(context);
            if (!(expression instanceof JLiteralInt)) {
                JAST.compilationUnit.reportSemanticError(line(),
                    "Invalid expression type for switch label: " + expression.type());
            }
        }
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
        output.addLabel(label);
    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        if (type == CASE) {
            json.addChild("Case", e);
            expression.toJSON(e);
        } else { // DEFLT
            json.addChild("Default", e);
        }
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public TokenKind type() {
        return type;
    }

    public int value() {
        return ((JLiteralInt) expression).value();
    }
}