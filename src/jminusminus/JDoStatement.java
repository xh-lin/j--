package jminusminus;

import static jminusminus.CLConstants.*;

/**
 * The AST node for a do statement.
 */
public class JDoStatement extends JStatement{
    private JStatement body;
    private JExpression condition;

    private String breakLabel;
    private String continueLabel;

    /**
     * Constructs an AST node for a do statement given its line number,
     *
     * @param line line in which the do statement occurs in the source file.
     * @param body the AST node for the body
     * @param condition the AST node for condition
     */
    public JDoStatement(int line, JStatement body, JExpression condition) {
        super(line);
        this.body = body;
        this.condition = condition;
    }

    /**
     * {@inheritDoc}
     */
    public JStatement analyze(Context context) {
        // for analyzing break and continue statements
        JMember.controlFlows.push((JStatement) this);

        condition = condition.analyze(context);
        condition.type().mustMatchExpected(line(), Type.BOOLEAN);
        body = (JStatement) body.analyze(context);

        // pop self out
        JMember.controlFlows.pop();

        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
        String loop = output.createLabel();
        String out = output.createLabel();
        setBreakLabel(out);
        setContinueLabel(loop);
        output.addLabel(loop);
        body.codegen(output);
        condition.codegen(output, loop, true);
        output.addLabel(out);
    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JDoStatement:" + line, e);
        JSONElement b = new JSONElement();
        e.addChild("Body", b);
        body.toJSON(b);
        JSONElement c = new JSONElement();
        e.addChild("Condition", c);
        condition.toJSON(c);
    }

    public void setBreakLabel(String breakLabel) {
        this.breakLabel = breakLabel;
    }

    public String getBreakLabel() {
        return this.breakLabel;
    }

    public void setContinueLabel(String continueLabel) {
        this.continueLabel = continueLabel;
    }

    public String getContinueLabel() {
        return this.continueLabel;
    }
}
