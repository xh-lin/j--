package jminusminus;

import static jminusminus.CLConstants.ATHROW;

/**
 * The AST node for a throw statement.
 */
class JThrowStatement extends JStatement {
    private JExpression exception;

    /**
     * Constructs an AST node for a throw statement given its line number,
     *
     * @param line line in which the throw statement occurs in the source file.
     * @param exception the AST node for the exception
     */
    public JThrowStatement(int line, JExpression exception) {
        super(line);
        this.exception = exception;
    }

    /**
     * {@inheritDoc}
     */
    public JStatement analyze(Context context) {
        exception = exception.analyze(context);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
        exception.codegen(output);
        output.addNoArgInstruction(ATHROW);
    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JThrowStatement:" + line, e);
        JSONElement expr = new JSONElement();
        e.addChild("Expression", expr);
        exception.toJSON(expr);
    }
}
