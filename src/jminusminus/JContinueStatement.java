package jminusminus;

import static jminusminus.CLConstants.GOTO;

/**
 * The AST node for a continue statement.
 */
class JContinueStatement extends JStatement {
    private JStatement enclosure;

    /**
     * Constructs an AST node for a continue statement given its line number,
     *
     * @param line line in which the continue statement occurs in the source file.
     */
    public JContinueStatement(int line) {
        super(line);
    }

    /**
     * {@inheritDoc}
     */
    public JStatement analyze(Context context) {
        enclosure = JMember.controlFlows.peek();
        if (enclosure == null) {
            JAST.compilationUnit.reportSemanticError(line(),
                    "continue statement is outside of a control flow");
        }
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void codegen(CLEmitter output) {
        String label = null;
        if (enclosure instanceof JDoStatement) {
            label = ((JDoStatement) enclosure).getContinueLabel();
        } else if (enclosure instanceof JWhileStatement) {
            label = ((JWhileStatement) enclosure).getContinueLabel();
        } else if (enclosure instanceof JForStatement) {
            label = ((JForStatement) enclosure).getContinueLabel();
        } else {
            JAST.compilationUnit.reportSemanticError(line(),
                    "continue statement should not be inside of " + enclosure.toString());
        }
        output.addBranchInstruction(GOTO, label);
    }

    /**
     * {@inheritDoc}
     */
    public void toJSON(JSONElement json) {
        JSONElement e = new JSONElement();
        json.addChild("JContinueStatement:" + line, e);
    }
}
