1. Enter the number of hours it took you to complete the project between
   the <<< and >>> signs below (eg, <<<10>>>).

   <<<5>>>
   
2. Enter the difficulty level (1: very easy; 5: very difficult) of the project
   between <<< and >>> signs below (eg, <<<3>>>).

   <<<1>>>

3. Provide a short description of how you approached each problem, issues you 
   encountered, and how you resolved those issues.

   Problem 1 (Multiline Comment)
      Approach: I drew a DFA for recognizing multiline comments, then wrote
      my code according to the state diagram.

      Issues and resolution: Took a while to think, but no problem.

   Problem 2 (Reserved Words)
      Approach: Just simply followed the checklist. Define tokens and modify
      the scanner to recognize them.

      Issues and resolution: I have done it fairly quick and no issues.

   Problem 3 (Operators)
      Approach: This problem is very similar to the previous one. I just
      define tokens, then modify the scanner to recognize the operators.

      Issues and resolution: It was easy and no issues.
   
   Problem 4 (Literals)
      Approach: I followed the checklist. Define the tokens, and modify the
      section of integer literals to also recognize long and double literals.

      Issues and resolution: I didn't pass Gradescope the first time because
      I didn't know that I have to also implement literals for EXPONENT and
      SUFFIX.

4. Did you receive help from anyone? List their names, status (classmate, 
   CS451/651 grad, TA, other), and the nature of help received.

   Name               Status       Help Received
   ----               ------       -------------

   None                ...          ...

5. List any other comments here. Feel free to provide any feedback on how
   much you learned from doing the assignment, and whether you enjoyed
   doing it.

   I now have a better understanding about how the scanner recognizes each
   tokens, and how to add support for new operators.
