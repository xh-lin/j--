1. Enter the number of hours it took you to complete the project between
   the <<< and >>> signs below (eg, <<<10>>>).

   <<<11>>>
   
2. Enter the difficulty level (1: very easy; 5: very difficult) of the project
   between the <<< and >>> signs below (eg, <<<3>>>).

   <<<3>>>

3. Provide a short description of how you approached each problem, issues you 
   encountered, and how you resolved those issues.

   Part I (Additions to JavaCC Scanner)

   Problem 1 (Multiline Comment): I looked at how the code for single line
   comment is written in j--, and my notes, then I understood how to make it
   jump to another state when seeing some strings, and skip anything else.
   I parsed the given test file to see if it was correct.

   Problem 2 (Reserved Words): I did a Control-F to search "reserved words" to
   found where all the reserved words were defined. I found that they are in
   TOKEN, so I added all the reserved words of this problem to TOKEN. I
   tokenized some test files to check the correctness.

   Problem 3 (Operators): I did Control-F to find where the tokens for operators
   are. I checked if the tokens for those operators were defined. If not, then
   defined those tokens in TOKEN under "// Operators". Then, used the test file
   to check.

   Problem 4 (Literals): I did Control-F to find where the tokens for literals
   are. I looked at lecture slides and my notes to review the notation, then
   tried to write the grammar from appendix into the notation of JavaCC. When I
   compile, it gave to an error because I used [...] for optional. I didn't know
   why, so I asked on Piazza and the professor told me to use (...)?.

   Part II (Additions to JavaCC Parser)
   
   Problem 5 (Long and Double Basic Types): I parsed the given test file after
   I done writting the code. I didn't know why all double types are parsed
   as long types for BasicTypes.java. I asked on Piazza and the professor
   suggests to modify Type.descriptorFor(), which I didn't know I needed to do.
   Then, the output was fine, but I got an error that says post-increment
   has no side-effect. I did a Control-F of "side-effect" to see which function
   is causing the issue, then I found out that I needed to modify the
   statementExpression() function to add the operators.

   Problem 6 (Operators): I used Control-F to find the name of the token of a
   operator, then find where that token has been used in the appendix, so I know
   where to modify. There was no conditionalOrExpression for "||", so I created
   a function for it. Checked with the test file and it was fine.
   
   Problem 7 (Conditional Expression): The conditionalExpression function didn't
   exist, so I created one, and I changed the function call in
   assignmentExpression to call conditionalExpression. I parsed the test file to
   check if it works or not, and it was fine.

   Problem 8 (Switch Statement): I modified the statement function, and created
   switchBlockStatementGroup and switchLabel. When I compiled it, I got an
   error that says "Expansion within "(...)*" can be matched by empty string."
   I found the issue was in switchBlockStatementGroup. After a fix, I had errors
   when parsing the test file, then I realize I did not add support to break
   statement yet. It worked after I added that.

   Problem 9 (Do-While Statement): I modified the statement() function to
   support the do statement. After coding, it compiled successfully and parsed
   the test file correctly.

   Problem 10 (For Statement): I modified the statement() to support
   for-statement. Then, I added functions forInit() and forUpdate(). After it
   was done, it compiled successfully and parsed the test file correctly.

   Problem 11 (Break Statement): I modified the statement() to support
   break-statement, which only contains two tokens. I parsed the test file to
   check, and it worked fine.

   Problem 12 (Continue Statement): I modified the statement() to support
   continue-statement, which is similar to break-statement. I parsed the test
   file after I compiled it successfully, and the output was correct.

   Problem 13 (Exception Handlers): I modified statement() to support TRY and
   THROW. memberDecl and interfaceMemberDecl need to support the THROWS clause,
   but I have done it when modifying j--.jj in the project 3. It compiled
   successfully, but had "... found where one of ... sought" errors. I fixed
   that in the TRY section, but then I got a null pointer exception error at
   JavaCCParser.memberDecl(). I found out that I forgot to initialize an
   array. After fixing, it worked fine.

   Problem 14 (Interface Type Declaration): In project 3 problem 10, I have
   already modified j--.jj and added interfaceDeclaration to fix compilation
   issues. Thus, I assumed that I didn't need to do anything. I parsed the test
   file and it worked as what I expected.

4. Did you receive help from anyone? List their names, status (classmate, 
   CS451/651 grad, TA, other), and the nature of help received.

   Name               Status       Help Received
   ----               ------       -------------

   Swami Iyer        professor      In problem4, use (...)? for optional.
   Swami Iyer        professor      In problem5, need to modify descriptorFor().

5. List any other comments here. Feel free to provide any feedback on how
   much you learned from doing the assignment, and whether you enjoyed
   doing it.

   This project is very much similar to what we have done in previous projects,
   and the notation used in j--.jj is intuitive. However, I don't like editing
   j--.jj because I feel like I am editing a plain text and it is easy for me
   to make a typo.
