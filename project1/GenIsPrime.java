import jminusminus.CLEmitter;

import java.util.ArrayList;

import static jminusminus.CLConstants.*;

/**
 * Xu Huang Lin
 * 9/22/2020
 *
 * This class programmatically generates the class file for the following Java application:
 *
 * <pre>
 * public class IsPrime {
 *     // Entry point.
 *     public static void main(String[] args) {
 *         int n = Integer.parseInt(args[0]);
 *         boolean result = isPrime(n);
 *         if (result) {
 *             System.out.println(n + " is a prime number");
 *         } else {
 *             System.out.println(n + " is not a prime number");
 *         }
 *     }
 *
 *     // Returns true if n is prime, and false otherwise.
 *     private static boolean isPrime(int n) {
 *         if (n < 2) {
 *             return false;
 *         }
 *         for (int i = 2; i <= n / i; i++) {
 *             if (n % i == 0) {
 *                 return false;
 *             }
 *         }
 *         return true;
 *     }
 * }
 * </pre>
 */
public class GenIsPrime {
    public static void main(String[] args) {
        // Create a CLEmitter instance
        CLEmitter e = new CLEmitter(true);

        // Create an ArrayList instance to store modifiers
        ArrayList<String> modifiers = new ArrayList<String>();

        // public class IsPrime {
        modifiers.add("public");
        e.addClass(modifiers, "IsPrime", "java/lang/Object",
                null, true);

        // public static void main(String[] args) {
        modifiers.clear();
        modifiers.add("public");
        modifiers.add("static");
        e.addMethod(modifiers, "main", "([Ljava/lang/String;)V",
                null, true);

        // int n = Integer.parseInt(args[0]);
        e.addNoArgInstruction(ALOAD_0); // push args[] reference
        e.addNoArgInstruction(ICONST_0); // push 0
        e.addNoArgInstruction(AALOAD); // pop above two; push value of args[0]
        e.addMemberAccessInstruction(INVOKESTATIC, "java/lang/Integer",
                "parseInt", "(Ljava/lang/String;)I"); // pop str; push int
        e.addNoArgInstruction(ISTORE_1); // store int to n

        // boolean result = isPrime(n);
        e.addNoArgInstruction(ILOAD_1); // push int from n
        e.addMemberAccessInstruction(INVOKESTATIC, "IsPrime", "isPrime", "(I)I");
        e.addNoArgInstruction(ISTORE_2); // store bool to result

        // Get System.out on stack
        e.addMemberAccessInstruction(GETSTATIC, "java/lang/System", "out",
                "Ljava/io/PrintStream;");

        // Create an instance (say sb) of StringBuffer on stack for string concatenations
        //    sb = new StringBuffer();
        e.addReferenceInstruction(NEW, "java/lang/StringBuffer");
        e.addNoArgInstruction(DUP);
        e.addMemberAccessInstruction(INVOKESPECIAL, "java/lang/StringBuffer", "<init>",
                "()V");

        // sb.append(n);
        e.addNoArgInstruction(ILOAD_1);
        e.addMemberAccessInstruction(INVOKEVIRTUAL, "java/lang/StringBuffer", "append",
                "(I)Ljava/lang/StringBuffer;");

        // if (!result) branch to "Isnt"
        e.addNoArgInstruction(ILOAD_2); // push bool from result
        e.addNoArgInstruction(ICONST_0); // push 0
        e.addBranchInstruction(IF_ICMPEQ, "Isnt"); // pop above two; jump to "Isnt" if false

        // sb.append(" is a prime number");
        e.addLDCInstruction(" is a prime number");
        e.addMemberAccessInstruction(INVOKEVIRTUAL, "java/lang/StringBuffer", "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuffer;");
        e.addBranchInstruction(GOTO, "End");

        // sb.append(" is not a prime number");
        e.addLabel("Isnt");
        e.addLDCInstruction(" is not a prime number");
        e.addMemberAccessInstruction(INVOKEVIRTUAL, "java/lang/StringBuffer", "append",
                "(Ljava/lang/String;)Ljava/lang/StringBuffer;");

        // System.out.println(sb.toString());
        e.addLabel("End");
        e.addMemberAccessInstruction(INVOKEVIRTUAL, "java/lang/StringBuffer",
                "toString", "()Ljava/lang/String;");
        e.addMemberAccessInstruction(INVOKEVIRTUAL, "java/io/PrintStream", "println",
                "(Ljava/lang/String;)V");

        // return;
        e.addNoArgInstruction(RETURN);

        // private static boolean isPrime(int n) {
        modifiers.clear();
        modifiers.add("private");
        modifiers.add("static");
        e.addMethod(modifiers, "isPrime", "(I)I", null, true);

        // if (n < 2) branch to "F"
        e.addNoArgInstruction(ILOAD_0); // push n
        e.addNoArgInstruction(ICONST_2); // push 2
        e.addBranchInstruction(IF_ICMPLT, "F");

        // for (int i = 2; i <= n / i; i++) {
        e.addNoArgInstruction(ICONST_2);
        e.addNoArgInstruction(ISTORE_1); // store 2 in i
        e.addLabel("Loop");
        e.addNoArgInstruction(ILOAD_0); // push n
        e.addNoArgInstruction(ILOAD_1); // push i
        e.addNoArgInstruction(IDIV); // pop above two; push n/i
        e.addNoArgInstruction(ILOAD_1); // push i
        e.addBranchInstruction(IF_ICMPLT, "T"); // break loop if n/i < i

        // if (n % i == 0) {
        e.addNoArgInstruction(ILOAD_0); // push n
        e.addNoArgInstruction(ILOAD_1); // push i
        e.addNoArgInstruction(IREM); // pop above two; push n%i

        e.addNoArgInstruction(ILOAD_1);
        e.addNoArgInstruction(ICONST_1);
        e.addNoArgInstruction(IADD);
        e.addNoArgInstruction(ISTORE_1); // i++

        e.addNoArgInstruction(ICONST_0);
        e.addBranchInstruction(IF_ICMPNE, "Loop"); // loop if n%i != 0

        // return false;
        e.addLabel("F");
        e.addNoArgInstruction(ICONST_0);
        e.addNoArgInstruction(IRETURN);

        // return true;
        e.addLabel("T");
        e.addNoArgInstruction(ICONST_1);
        e.addNoArgInstruction(IRETURN);

        // Write IsPrime.class to file system
        e.write();
    }
}